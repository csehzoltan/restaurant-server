package org.restaurant.server.main;

import java.io.IOException;

import org.restaurant.server.socket.RestaurantServer;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Megjeleníti a felhasználó felületet és elindítja a szerver oldali kommunikációt, fogadja a klienseket, azok kéréseit, végrehajtja azokat
 * és választ küld amennyiben szükséges.
 * @author cseh.zoltan
 *
 */
public class RestaurantMain extends Application {
	
	private RestaurantServer resServ;
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		startGUI(primaryStage);
		startServer();
	}

	private Scene startGUI(Stage primaryStage) throws IOException {
		primaryStage.setTitle("Server");
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/fxml.common/Main.fxml"));
		Parent root = loader.load();
		Scene scene = new Scene(root);
		scene.getStylesheets().add(RestaurantMain.class.getResource("/gui.css/main.css").toExternalForm());
		primaryStage.setScene(scene);
		//primaryStage.setFullScreen(true);
		primaryStage.show();

		primaryStage.setOnCloseRequest(e -> {
			resServ.shutDown();
			Platform.exit();
		});

		return scene;
	}

	private void startServer() {
		resServ = new RestaurantServer();
		Thread server = new Thread(resServ);
		server.start();
	}
}
