package org.restaurant.server.controller;

import com.jfoenix.controls.JFXTreeTableView;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

/**
 * GUI kezdeményezés szerver oldalon, nincs jelentősége, a rendelések kiírására lett volna létrehozva, a dokumentáció nem 
 * tartalmazza ezen igényt, idő keretein belül nem fért volna bele, ezért csak ennyi lett megvalósítva hozzá.
 * @author cseh.zoltan
 *
 */
public class MainController {

	@FXML
	private Label testLabel;

    @FXML
    private BorderPane mainBorderPane;

    @FXML
    private JFXTreeTableView<?> orderTableView;

}
