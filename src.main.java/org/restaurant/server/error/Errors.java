package org.restaurant.server.error;

public enum Errors {
	ERROR_GENERAL_ERROR(0, "Internal error! Contact the staff!");
	
	private int id;
	private String display;

	Errors(int id, String display) {
		this.id = id;
		this.display = display;
	}

	public String getDisplay() {
		return display;
	}

	public int getId() {
		return id;
	}
}
