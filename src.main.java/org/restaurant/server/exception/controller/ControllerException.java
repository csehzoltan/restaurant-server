package org.restaurant.server.exception.controller;

public class ControllerException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6067466668078422862L;

	public ControllerException() {
		super();
	}

	public ControllerException(String arg0) {
		super(arg0);
	}

	public ControllerException(Throwable arg0) {
		super(arg0);
	}

	public ControllerException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ControllerException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}
}
