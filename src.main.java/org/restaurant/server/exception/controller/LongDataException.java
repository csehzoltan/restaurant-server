package org.restaurant.server.exception.controller;

public class LongDataException extends ControllerException {
	
	private static final long serialVersionUID = 4760158105550915744L;
	
	public LongDataException() {
		super();
	}

	public LongDataException(String arg0) {
		super(arg0);
	}
}
