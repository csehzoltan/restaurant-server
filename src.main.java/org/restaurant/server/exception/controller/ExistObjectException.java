package org.restaurant.server.exception.controller;

import java.util.ArrayList;
import java.util.List;

public class ExistObjectException extends ControllerException {

	private static final long serialVersionUID = -4099222154282581637L;
	private final List<String> parameters;
	
	public ExistObjectException() {
		super();
		this.parameters=null;
	}
	
	public ExistObjectException(Throwable arg0) {
		super(arg0);
		this.parameters = null;
	}
	
	public ExistObjectException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		this.parameters=null;
	}

	public ExistObjectException(String arg0, String... params) {
		super(arg0);
		this.parameters = new ArrayList<>();
		for (String param : params) {
			parameters.add(param);
		}
	}

	public ExistObjectException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);
		this.parameters=null;
	}
	
	public List<String> getParameters() {
		return parameters;
	}
}
