package org.restaurant.server.exception.controller;

public class MissingBeanControllerException extends ControllerException {

	private static final long serialVersionUID = 9048699632803925741L;

	public MissingBeanControllerException() {
		super();
	}

	public MissingBeanControllerException(String arg0) {
		super(arg0);
	}

	public MissingBeanControllerException(Throwable arg0) {
		super(arg0);
	}

}
