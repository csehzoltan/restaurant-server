package org.restaurant.server.data.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.restaurant.common.model.beans.CoordXY;
import org.restaurant.common.model.beans.Ingredient;
import org.restaurant.common.model.beans.Product;
import org.restaurant.common.model.beans.ProductCategory;
import org.restaurant.common.model.beans.ProductIngredient;
import org.restaurant.common.model.beans.ProductOrder;
import org.restaurant.common.model.beans.ProductPrice;
import org.restaurant.common.model.beans.ProductRequest;
import org.restaurant.common.model.beans.Table;

public interface ProductMapper {
	Product selectProduct(@Param("id") int id);
	List<Product> selectProducts(@Param("map") Map<String, Object> productFilter);
	void insertProduct(Product product);
	void updateProduct(Product product);
	void deleteProduct(Product product);
	
	ProductCategory selectProductCategory(@Param("id") int id);
	
	ProductPrice selectProductPrice(@Param("id") int id);
	List<ProductPrice> selectProductPrices(@Param("id") int id);
	void insertProductPrice(ProductPrice productPrice);
	void updateProductPrice(ProductPrice productPrice);
	void deleteProductPrice(ProductPrice productPrice);
	
	ProductRequest selectProductRequest(@Param("id") int id);
	List<ProductRequest> selectProductRequestByConditions(@Param("map") Map<String, Object> map);
	ProductRequest selectProductRequestTest(@Param("id") int id);
	void insertProductRequest(ProductRequest productRequest);
	void updateProductRequest(ProductRequest productRequest);
	void deleteProductRequest(ProductRequest productRequest);
	
	ProductOrder selectProductOrder(@Param("id") int id);
	void insertProductOrder(ProductOrder productOrder);
	void updateProductOrder(ProductOrder productOrder);
	
	Ingredient selectIngredient(@Param("id") int id);
	void insertIngredient(Ingredient ingredient);
	void updateIngredient(Ingredient ingredient);
	void deleteIngredient(Ingredient ingredient);
	List<Ingredient> selectIngredients(@Param("map") Map<String, Object> map);
	List<Ingredient> selectProductIngredients(@Param("product_id") int productId);
	void deleteProductIngredient(@Param("productIngredientId") int productIngredientId);
	void deleteProductIngredientByProdAndIngredId(@Param("productId") int productId, @Param("ingredientId") int ingredientId);
	void updateProductIngredientByProdAndIngredientId(ProductIngredient productIngredient);
	void insertProductIngredient(ProductIngredient productIngredient);
	
	Table selectTable(@Param("id") int id);
	
	List<CoordXY> selectProductStatisticsIncomes(@Param("res") Map<String, Object> res);
	List<CoordXY> selectProductCategoryStatistics(@Param("res") Map<String, Object> res);
}
