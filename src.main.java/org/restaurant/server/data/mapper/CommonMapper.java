package org.restaurant.server.data.mapper;

import org.apache.ibatis.annotations.Param;
import org.restaurant.common.model.beans.Chair;
import org.restaurant.common.model.beans.Table;

public interface CommonMapper {
	Table selectTable(@Param("id") int id);
	Chair selectChair(@Param("id") int id);
	
	Chair selectChairByTableIdAndChairName(@Param("tableId") int tableId, @Param("chairName") String chairName);
}
