package org.restaurant.server.data.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.restaurant.common.model.beans.CoordXY;
import org.restaurant.common.model.beans.Guestbook;
import org.restaurant.common.model.beans.User;

public interface UserMapper {
	User selectUser(@Param("id") int id);
	List<User> selectUsers();
	User selectUserByLoginData(@Param("user") User user, @Param("map") Map<String,Object> filter);
	void insertUser(User user);
	void updateUser(User user);
	void deleteUser(User user);
	
	User selectWaiter(User user);
	User selectUserByAccountName(@Param("accountName") String accountName);
	
	Guestbook selectGuestbook(@Param("id") int id);
	List<Guestbook> selectGuestbooks();
	List<Guestbook> selectGuestBookAdministrator(@Param("res") Map<String, Object> res);
	void insertGuestbook(Guestbook guestbook);
	void updateGuestbook(Guestbook guestbook);
	void deleteGuestbook(Guestbook guestbook);
}
