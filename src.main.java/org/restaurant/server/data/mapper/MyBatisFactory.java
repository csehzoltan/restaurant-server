package org.restaurant.server.data.mapper;

import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.restaurant.server.properties.PropertyException;
import org.restaurant.server.properties.ServerConfig;

public class MyBatisFactory {
	private static SqlSessionFactory factory;
	private static ServerConfig serverProperty;
	
	static {
		String configPath = "mybatis/config2.xml";
		try {
			serverProperty = new ServerConfig();
			if(!serverProperty.isLocalhostDB()) {
				configPath = "mybatis/config.xml";
			}
		} catch (PropertyException e1) {
			e1.printStackTrace();
		}
		try (Reader reader = Resources.getResourceAsReader(configPath);) {
			factory = new SqlSessionFactoryBuilder().build(reader);
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}

	public static SqlSessionFactory getSqlSessionFactory() {
		return factory;
	}
}
