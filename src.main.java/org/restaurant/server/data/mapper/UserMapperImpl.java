package org.restaurant.server.data.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.restaurant.common.model.beans.Guestbook;
import org.restaurant.common.model.beans.User;

/**
 * Controllerek és DAO-k tesztelésére létrehozott osztály, fontossága, hogy adatbázis nélkül is lehet tesztelni vele.
 * @author cseh.zoltan
 *
 */
public class UserMapperImpl implements UserMapper {

	@Override
	public User selectUser(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User selectUserByLoginData(User user, @Param("map") Map<String,Object> filter) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertUser(User user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteUser(User user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public User selectWaiter(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> selectUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User selectUserByAccountName(String accountName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Guestbook selectGuestbook(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Guestbook> selectGuestbooks() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertGuestbook(Guestbook guestbook) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateGuestbook(Guestbook guestbook) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteGuestbook(Guestbook guestbook) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Guestbook> selectGuestBookAdministrator(Map<String, Object> res) {
		// TODO Auto-generated method stub
		return null;
	}

}
