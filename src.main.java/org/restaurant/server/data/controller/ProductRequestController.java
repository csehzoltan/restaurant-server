package org.restaurant.server.data.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.restaurant.common.model.beans.PayType;
import org.restaurant.common.model.beans.ProductRequest;
import org.restaurant.common.model.beans.ProductRequestState;
import org.restaurant.common.model.beans.Table;
import org.restaurant.common.model.beans.User;
import org.restaurant.server.data.dao.ProductRequestDAO;
import org.restaurant.server.data.mapper.ProductMapper;
import org.restaurant.server.exception.controller.MissingBeanControllerException;

public class ProductRequestController extends AbstractController<ProductRequest, ProductRequestDAO> {
	
	public ProductRequestController(ProductMapper mapper) {
		super();
		dao = new ProductRequestDAO(mapper);
	}

	public ProductRequestController(ProductMapper mapper, ProductRequest bean) {
		super();
		dao = new ProductRequestDAO(mapper);
		setBean(bean);
	}

	public ProductRequestController(ProductMapper mapper, Integer id) throws MissingBeanControllerException {
		super();
		dao = new ProductRequestDAO(mapper);
		get(id);
	}
	
	/**
	 * Létrehoz egy új kérést, amennyiben az asztalhoz nincs nyitott kérés, vagy kikapcsoljuk a létező kérés ellenőrzését.
	 * @param requestedById vevő azonosítója.
	 * @param waiterId felszolgáló azonosítója.
	 * @param tableId asztal azonosítója.
	 * @param checkExistRequestToTable létező kérés ellenőrzése, hogy van-e nyitott kérés ehhez az asztalhoz (ebben az esetben ha van, akkor azt töltjük be).
	 */
	public void createNewRequest(Integer requestedById, Integer waiterId, Integer tableId, boolean checkExistRequestToTable) {
		
		ProductRequest productRequest = new ProductRequest();
		List<ProductRequest> existRequest = null;
		if(checkExistRequestToTable) {
			Map<String, Object> tableAndStatusFilter = new HashMap<>();
			tableAndStatusFilter.put("tableId", String.valueOf(tableId));
			tableAndStatusFilter.put("stateId", String.valueOf(ProductRequestState.OPEN.getId()));
			existRequest = dao.listFiltered(tableAndStatusFilter);
		}
		
		if(!checkExistRequestToTable || existRequest == null || existRequest.isEmpty()) {
			User requestedBy = new User();
			requestedBy.setId(requestedById);
			
			User waiter = new User();
			waiter.setId(waiterId);
			
			Table table = new Table();
			table.setId(tableId);
			
			productRequest.setState(ProductRequestState.OPEN);
			productRequest.setRequestedBy(requestedBy);
			productRequest.setRequestedAt(new Date());
			productRequest.setTable(table);
			productRequest.setWaiter(waiter);
			setBean(productRequest);
		} else {
			setBean(existRequest.get(0));
		}
		
	}
	
	/**
	 * Visszatér a megadott filterek alapján betöltött kérésekkel.
	 * @param filter lehetséges kulcsok: waiterId, tableId, stateId, fromDate, getNoWaiterRequest, waiterIds.
	 * @return szűrésnek megfelelő kérésekkel.
	 */
	public List<ProductRequest> getProductRequestsByFilter(Map<String, Object> filter) {
		return dao.listFiltered(filter);
	}
	
	/**
	 * Beállítja a kérés státuszát payingre és a fizetés típusát.
	 * @param payType fizetés típusa (egyvalaki, mindenki külön).
	 */
	public void paying(PayType payType) {
		if(bean != null) {
			bean.setPayType(payType);
			bean.setState(ProductRequestState.PAYING);
		}
	}
	
	/**
	 * Felszolgálóhoz rendeli a megrendelést. Amennyiben van olyan felszolgáló, aki már a megadott asztalhoz tartozik, akkor őt választja, 
	 * különben azon felszolgálót, akinek a legkevesebb nyitott státuszú megrendelése van, 
	 * amennyiben az előzőek alapján nem lehet dönteni, akkor a mai napon legkevesebb megrendelést teljesítőt választja.
	 * @param tableId asztal, ahonnan a rendelés érkezett.
	 * @param guestId vendég azonosítója.
	 * @param loggedInWaiters jelenleg bejelentkezett felhasználók, akik között ki lehet osztani a rendelést.
	 * @return visszatér a kiválasztott pincér azonosítójával.
	 */
	public Integer createRequestToWaiter(Integer tableId, Integer guestId, List<Integer> loggedInWaiters) {
		Map<String, Object> tableAndStatusFilter = new HashMap<>();
		tableAndStatusFilter.put("tableId", String.valueOf(tableId));
		tableAndStatusFilter.put("stateId", String.valueOf(ProductRequestState.OPEN.getId()));
		List<ProductRequest> request = dao.getMapper().selectProductRequestByConditions(tableAndStatusFilter);
		Integer selectedWaiterId = null;
		if(request == null || request.isEmpty()) {
			int actOpenedReqNo = Integer.MAX_VALUE;
			int actClosedReqNo = Integer.MIN_VALUE;
			for(Integer waiterId : loggedInWaiters) {
				HashMap<String, Object> requestFilters = new HashMap<>();
				//requestFilters.put("fromDate", DateHelper.getToday(0));
				requestFilters.put("waiterId", waiterId);
				int waiterOpenedReqNo = 0;
				int waiterClosedReqNo = 0;
				List<ProductRequest> todayRequestsForWaiter = dao.getMapper().selectProductRequestByConditions(requestFilters);
				for(ProductRequest req : todayRequestsForWaiter) {
					if(req.getState().equals(ProductRequestState.OPEN)) waiterOpenedReqNo++;
					if(req.getState().equals(ProductRequestState.CLOSED)) waiterClosedReqNo++;
				}
				if(waiterOpenedReqNo < actOpenedReqNo) {
					selectedWaiterId = waiterId;
					actOpenedReqNo = waiterOpenedReqNo;
					actClosedReqNo = waiterClosedReqNo;
				} else if((waiterOpenedReqNo == actOpenedReqNo) && (waiterClosedReqNo < actClosedReqNo)) {
					selectedWaiterId = waiterId;
					actOpenedReqNo = waiterOpenedReqNo;
					actClosedReqNo = waiterClosedReqNo;
				}
			}
			createNewRequest(guestId, selectedWaiterId, tableId, false);
		} else {
			selectedWaiterId = request.get(0).getWaiter().getId();
			setBean(request.get(0));
		}
		return selectedWaiterId;
	}
}
