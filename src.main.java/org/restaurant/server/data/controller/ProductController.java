package org.restaurant.server.data.controller;

import java.util.List;
import java.util.Map;

import org.restaurant.common.model.beans.Product;
import org.restaurant.server.data.dao.ProductDAO;
import org.restaurant.server.data.mapper.ProductMapper;
import org.restaurant.server.exception.controller.MissingBeanControllerException;

public class ProductController extends AbstractController<Product, ProductDAO> {
	
	public ProductController(ProductMapper mapper) {
		super();
		dao = new ProductDAO(mapper);
	}

	public ProductController(ProductMapper mapper, Product bean) {
		super();
		dao = new ProductDAO(mapper);
		setBean(bean);
	}

	public ProductController(ProductMapper mapper, Integer id) throws MissingBeanControllerException {
		super();
		dao = new ProductDAO(mapper);
		get(id);
	}
	
	public List<Product> getProducts(Map<String, Object> productFilter) {
		return dao.selectProducts(productFilter);
	}
}
