package org.restaurant.server.data.controller;

import org.restaurant.common.model.beans.Table;
import org.restaurant.server.data.dao.TableDAO;
import org.restaurant.server.data.mapper.CommonMapper;
import org.restaurant.server.exception.controller.MissingBeanControllerException;

public class TableController extends AbstractController<Table, TableDAO> {
	
	public TableController(CommonMapper mapper) {
		super();
		this.dao = new TableDAO(mapper);
	}
	
	public TableController(CommonMapper mapper, Table bean) {
		super();
		dao = new TableDAO(mapper);
		setBean(bean);
	}
	
	public TableController(CommonMapper mapper, int id) throws MissingBeanControllerException {
		super();
		dao = new TableDAO(mapper);
		get(id);
	}
}
