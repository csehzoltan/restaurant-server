package org.restaurant.server.data.controller;

import java.util.List;
import java.util.Map;

import org.restaurant.common.model.beans.User;
import org.restaurant.server.data.dao.UserDAO;
import org.restaurant.server.data.mapper.UserMapper;
import org.restaurant.server.exception.controller.ControllerException;
import org.restaurant.server.exception.controller.ExistObjectException;
import org.restaurant.server.exception.controller.MissingBeanControllerException;

public class UserController extends AbstractController<User, UserDAO> {
	public UserController(UserMapper mapper, int id) throws MissingBeanControllerException {
		super();
		this.dao = new UserDAO(mapper);
		get(id);
	}
	
	public UserController(UserMapper mapper, User bean) {
		super();
		dao = new UserDAO(mapper);
		setBean(bean);
	}
	
	public UserController(UserMapper mapper) {
		this.dao = new UserDAO(mapper);
	}
	
	public void getUserByLoginData(User user, Map<String, Object> filter) {
		setBean(dao.selectUserByLoginData(user, filter));
	}
	
	public List<User> getUsers() {
		return dao.getUsers();
	}
	
	@Override
	public void save() throws ControllerException {
		if(bean.getId() == null && dao.getByAccountName(bean.getAccountName())!=null) {
			throw new ExistObjectException("Exist account name!"); 
		}
		super.save();
	}
}
