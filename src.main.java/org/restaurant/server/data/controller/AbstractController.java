package org.restaurant.server.data.controller;

import org.restaurant.common.model.beans.SimpleBean;
import org.restaurant.server.data.dao.AbstractDAO;
import org.restaurant.server.exception.controller.ControllerException;
import org.restaurant.server.exception.controller.MissingBeanControllerException;

public class AbstractController<T extends SimpleBean, D extends AbstractDAO<T, ?>> {
	protected T bean;
	protected D dao;

	public void get(Integer id) throws MissingBeanControllerException {
		bean = dao.get(id);
		if (bean == null) {
			throw new MissingBeanControllerException("The bean is not exists!");
		}
	}

	public void save() throws ControllerException {
		try {
			dao.save(bean);
		} catch (Exception e) {
			throw new ControllerException(e);
		}

	}

	public void delete(T bean) {
		dao.delete(bean);
	}

	public T getBean() {
		return this.bean;
	}

	public void setBean(T bean) {
		this.bean = bean;
	}

	public D getDao() {
		return dao;
	}

	public void setDao(D dao) {
		this.dao = dao;
	}
}
