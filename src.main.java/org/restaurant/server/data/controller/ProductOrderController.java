package org.restaurant.server.data.controller;

import org.restaurant.common.model.beans.Chair;
import org.restaurant.common.model.beans.ProductOrder;
import org.restaurant.common.model.beans.ProductOrderState;
import org.restaurant.common.model.beans.ProductPrice;
import org.restaurant.server.data.dao.ProductOrderDAO;
import org.restaurant.server.data.mapper.ProductMapper;
import org.restaurant.server.exception.controller.MissingBeanControllerException;

public class ProductOrderController extends AbstractController<ProductOrder, ProductOrderDAO>   {
	
	public ProductOrderController(ProductMapper mapper) {
		super();
		dao = new ProductOrderDAO(mapper);
	}

	public ProductOrderController(ProductMapper mapper, ProductOrder bean) {
		super();
		dao = new ProductOrderDAO(mapper);
		setBean(bean);
	}

	public ProductOrderController(ProductMapper mapper, Integer id) throws MissingBeanControllerException {
		super();
		dao = new ProductOrderDAO(mapper);
		get(id);
	}
	
	public void createNewProductOrder(Integer productRequestId, Integer productPriceId, Integer quantity, int rejectedQuantity, 
			Integer chairId) {
		ProductOrder productOrder = new ProductOrder();
		productOrder.setProductRequestId(productRequestId);
		ProductPrice productPrice = new ProductPrice();
		productPrice.setId(productPriceId);
		productOrder.setProductPrice(productPrice);
		productOrder.setQuantity(quantity);
		productOrder.setRejectedQuantity(rejectedQuantity);
		Chair chair = new Chair();
		chair.setId(chairId);
		productOrder.setChair(chair);
		productOrder.setState(ProductOrderState.INPROGRESS);
		setBean(productOrder);
	}
}
