package org.restaurant.server.data.controller;

import java.util.List;
import java.util.Map;

import org.restaurant.common.model.beans.Ingredient;
import org.restaurant.server.data.dao.IngredientDAO;
import org.restaurant.server.data.mapper.ProductMapper;
import org.restaurant.server.exception.controller.MissingBeanControllerException;

public class IngredientController extends AbstractController<Ingredient, IngredientDAO> {
	
	public IngredientController(ProductMapper mapper) {
		super();
		dao = new IngredientDAO(mapper);
	}

	public IngredientController(ProductMapper mapper, Ingredient bean) {
		super();
		dao = new IngredientDAO(mapper);
		setBean(bean);
	}

	public IngredientController(ProductMapper mapper, Integer id) throws MissingBeanControllerException {
		super();
		dao = new IngredientDAO(mapper);
		get(id);
	}
	
	public List<Ingredient> getIngredients() {
		return dao.list();
	}
	
	public List<Ingredient> getIngredientsFiltered(Map<String, Object> filter) {
		return dao.listFiltered(filter);
	}
}
