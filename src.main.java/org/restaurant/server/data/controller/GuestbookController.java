package org.restaurant.server.data.controller;

import java.util.List;

import org.restaurant.common.model.beans.Guestbook;
import org.restaurant.server.data.dao.GuestbookDAO;
import org.restaurant.server.data.mapper.UserMapper;
import org.restaurant.server.exception.controller.ControllerException;
import org.restaurant.server.exception.controller.LongDataException;

public class GuestbookController extends AbstractController<Guestbook, GuestbookDAO> {
	public GuestbookController(UserMapper mapper, int id) {
		this.dao = new GuestbookDAO(mapper);
		this.dao.get(id);
	}
	
	public GuestbookController(UserMapper mapper, Guestbook guestbook) {
		super();
		dao = new GuestbookDAO(mapper);
		setBean(guestbook);
	}
	
	public GuestbookController(UserMapper mapper) {
		this.dao = new GuestbookDAO(mapper);
	}
	
	public List<Guestbook> getGuestbooks() {
		return dao.list();
	}
	
	@Override
	public void save() throws ControllerException {
		if(bean.getRemark().length() > 2000) {
			throw new LongDataException("The entered text is too long!"); 
		}
		super.save();
	}
}
