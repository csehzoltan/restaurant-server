package org.restaurant.server.data.controller;

import org.restaurant.common.model.beans.ProductPrice;
import org.restaurant.server.data.dao.ProductPriceDAO;
import org.restaurant.server.data.mapper.ProductMapper;
import org.restaurant.server.exception.controller.MissingBeanControllerException;

public class ProductPriceController extends AbstractController<ProductPrice, ProductPriceDAO> {
	public ProductPriceController(ProductMapper mapper) {
		super();
		dao = new ProductPriceDAO(mapper);
	}

	public ProductPriceController(ProductMapper mapper, ProductPrice bean) {
		super();
		dao = new ProductPriceDAO(mapper);
		setBean(bean);
	}

	public ProductPriceController(ProductMapper mapper, Integer id) throws MissingBeanControllerException {
		super();
		dao = new ProductPriceDAO(mapper);
		get(id);
	}
	
}
