package org.restaurant.server.data.dao;

import java.util.List;

import org.restaurant.common.model.beans.ProductOrder;
import org.restaurant.server.data.mapper.ProductMapper;

public class ProductOrderDAO extends AbstractDAO<ProductOrder, ProductMapper> {

	public ProductOrderDAO(ProductMapper mapper) {
		super(mapper);
	}

	@Override
	public void delete(ProductOrder bean) {
		// TODO Auto-generated method stub
	}

	@Override
	public ProductOrder get(int id) {
		return mapper.selectProductOrder(id);
	}

	@Override
	public List<ProductOrder> list() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void insert(ProductOrder bean) {
		mapper.insertProductOrder(bean);
	}

	@Override
	protected void update(ProductOrder bean) {
		mapper.updateProductOrder(bean);
	}

}
