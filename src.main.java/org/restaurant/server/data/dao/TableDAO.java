package org.restaurant.server.data.dao;

import java.util.List;

import org.restaurant.common.model.beans.Table;
import org.restaurant.server.data.mapper.CommonMapper;

public class TableDAO extends AbstractDAO<Table, CommonMapper> {

	public TableDAO(CommonMapper mapper) {
		super(mapper);
	}

	@Override
	public void delete(Table bean) {
		// TODO Auto-generated method stub
	}

	@Override
	public Table get(int id) {
		return mapper.selectTable(id);
	}

	@Override
	public List<Table> list() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void insert(Table bean) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void update(Table bean) {
		// TODO Auto-generated method stub
		
	}

}
