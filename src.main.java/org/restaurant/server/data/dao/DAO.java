package org.restaurant.server.data.dao;

import java.util.List;

import org.restaurant.common.model.beans.SimpleBean;

public interface DAO <T extends SimpleBean> {
	public void save(T bean);

	public T get(int id);
	
	public void delete(T bean);

	public List<T> list();

}
