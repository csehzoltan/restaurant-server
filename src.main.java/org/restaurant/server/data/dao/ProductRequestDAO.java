package org.restaurant.server.data.dao;

import java.util.List;
import java.util.Map;

import org.restaurant.common.model.beans.ProductRequest;
import org.restaurant.server.data.mapper.ProductMapper;

public class ProductRequestDAO extends AbstractDAO<ProductRequest, ProductMapper> {

	public ProductRequestDAO(ProductMapper mapper) {
		super(mapper);
	}

	@Override
	public void delete(ProductRequest bean) {
		mapper.deleteProductRequest(bean);
	}

	@Override
	public ProductRequest get(int id) {
		return mapper.selectProductRequest(id);
	}

	@Override
	public List<ProductRequest> list() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Visszatér a megadott filterek alapján betöltött kérésekkel.
	 * @param filter lehetséges kulcsok: waiterId, tableId, stateId, fromDate, getNoWaiterRequest, waiterIds.
	 * @return szűrésnek megfelelő kérésekkel.
	 */
	public List<ProductRequest> listFiltered(Map<String, Object> filter) {
		return mapper.selectProductRequestByConditions(filter);
	}

	@Override
	protected void insert(ProductRequest bean) {
		mapper.insertProductRequest(bean);
	}

	@Override
	protected void update(ProductRequest bean) {
		mapper.updateProductRequest(bean);
	}

}
