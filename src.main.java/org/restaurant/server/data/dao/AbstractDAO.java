package org.restaurant.server.data.dao;

import org.restaurant.common.model.beans.SimpleBean;

public abstract class AbstractDAO<T extends SimpleBean,S> implements DAO<T>{
	protected S mapper;
	
	public AbstractDAO(S mapper) {
		super();
		this.mapper = mapper;
	}
	
	@Override
	public void save(T bean) {
		if (bean.getId() == null) {
			insert(bean);
		} else {
			update(bean);
		}
	}

	protected abstract void insert(T bean);

	protected abstract void update(T bean);
	
	public S getMapper() {
		return mapper;
	}

}
