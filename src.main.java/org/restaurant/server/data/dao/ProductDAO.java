package org.restaurant.server.data.dao;

import java.util.List;
import java.util.Map;

import org.restaurant.common.model.beans.Product;
import org.restaurant.server.data.mapper.ProductMapper;

public class ProductDAO extends AbstractDAO<Product, ProductMapper> {

	public ProductDAO(ProductMapper mapper) {
		super(mapper);
	}
	
	@Override
	public Product get(int id) {
		return mapper.selectProduct(id);
	}

	@Override
	public void insert(Product bean) {
		mapper.insertProduct(bean);
	}

	@Override
	public void update(Product bean) {
		mapper.updateProduct(bean);
	}

	@Override
	public void delete(Product bean) {
		mapper.deleteProduct(bean);
	}
	
	public List<Product> selectProducts(Map<String, Object> productFilter) {
		return mapper.selectProducts(productFilter);
	}

	@Override
	public List<Product> list() {
		// TODO Auto-generated method stub
		return null;
	}

}
