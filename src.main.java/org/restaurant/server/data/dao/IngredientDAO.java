package org.restaurant.server.data.dao;

import java.util.List;
import java.util.Map;

import org.restaurant.common.model.beans.Ingredient;
import org.restaurant.server.data.mapper.ProductMapper;

public class IngredientDAO extends AbstractDAO<Ingredient, ProductMapper>  {

	public IngredientDAO(ProductMapper mapper) {
		super(mapper);
	}

	@Override
	public Ingredient get(int id) {
		return mapper.selectIngredient(id);
	}

	@Override
	public void delete(Ingredient bean) {
		mapper.deleteIngredient(bean);
	}

	@Override
	public List<Ingredient> list() {
		return mapper.selectIngredients(null);
	}
	
	public List<Ingredient> listFiltered(Map<String, Object> filter) {
		return mapper.selectIngredients(filter);
	}

	@Override
	protected void insert(Ingredient bean) {
		mapper.insertIngredient(bean);
	}

	@Override
	protected void update(Ingredient bean) {
		mapper.updateIngredient(bean);
	}

}
