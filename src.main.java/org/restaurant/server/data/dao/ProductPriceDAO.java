package org.restaurant.server.data.dao;

import java.util.List;

import org.restaurant.common.model.beans.ProductPrice;
import org.restaurant.server.data.mapper.ProductMapper;

public class ProductPriceDAO extends AbstractDAO<ProductPrice, ProductMapper> {
	public ProductPriceDAO(ProductMapper mapper) {
		super(mapper);
	}
	
	@Override
	public ProductPrice get(int id) {
		return mapper.selectProductPrice(id);
	}

	@Override
	public void insert(ProductPrice bean) {
		mapper.insertProductPrice(bean);
	}

	@Override
	public void update(ProductPrice bean) {
		mapper.updateProductPrice(bean);
	}

	@Override
	public void delete(ProductPrice bean) {
		mapper.deleteProductPrice(bean);
	}

	@Override
	public List<ProductPrice> list() {
		// TODO Auto-generated method stub
		return null;
	}
}
