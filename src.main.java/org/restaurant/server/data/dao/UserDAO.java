package org.restaurant.server.data.dao;

import java.util.List;
import java.util.Map;

import org.restaurant.common.model.beans.User;
import org.restaurant.server.data.mapper.UserMapper;

public class UserDAO extends AbstractDAO<User, UserMapper> {
	
	public UserDAO(UserMapper mapper) {
		super(mapper);
	}
	
	@Override
	public User get(int id) {
		return mapper.selectUser(id);
	}

	@Override
	public void delete(User bean) {
		mapper.deleteUser(bean);
	}

	@Override
	public void insert(User bean) {
		mapper.insertUser(bean);	
	}

	@Override
	public void update(User bean) {
		mapper.updateUser(bean);
	}

	public User selectUserByLoginData(User user, Map<String, Object> filter) {
		return mapper.selectUserByLoginData(user, filter);
	}
	
	public List<User> getUsers() {
		return mapper.selectUsers();
	}
	
	public User getByAccountName(String accountName) {
		return mapper.selectUserByAccountName(accountName);
	}

	@Override
	public List<User> list() {
		// TODO Auto-generated method stub
		return null;
	}
}
