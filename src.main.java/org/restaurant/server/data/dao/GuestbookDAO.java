package org.restaurant.server.data.dao;

import java.util.List;

import org.restaurant.common.model.beans.Guestbook;
import org.restaurant.server.data.mapper.UserMapper;

public class GuestbookDAO extends AbstractDAO<Guestbook, UserMapper>  {

	public GuestbookDAO(UserMapper mapper) {
		super(mapper);
	}

	@Override
	public Guestbook get(int id) {
		return mapper.selectGuestbook(id);
	}

	@Override
	public void delete(Guestbook bean) {
		mapper.deleteGuestbook(bean);
	}

	@Override
	public List<Guestbook> list() {
		return mapper.selectGuestbooks();
	}

	@Override
	protected void insert(Guestbook bean) {
		mapper.insertGuestbook(bean);
	}

	@Override
	protected void update(Guestbook bean) {
		mapper.updateGuestbook(bean);
	}

}
