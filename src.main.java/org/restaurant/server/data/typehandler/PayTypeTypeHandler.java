package org.restaurant.server.data.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;
import org.restaurant.common.model.beans.PayType;

/**
 * XML mapperhez létrehozott típus kezelő, enumoknál szükséges.
 * @author cseh.zoltan
 *
 */
public class PayTypeTypeHandler  implements TypeHandler<PayType> {
	
	@Override
	public PayType getResult(ResultSet rs, String param) throws SQLException {
		return PayType.getById(rs.getInt(param));
	}

	@Override
	public PayType getResult(CallableStatement cs, int col) throws SQLException {
		return PayType.getById(cs.getInt(col));
	}

	@Override
	public void setParameter(PreparedStatement ps, int paramInt, PayType paramType, JdbcType jdbctype)
			throws SQLException {
		Integer paramTypeId = paramType != null ? paramType.getId() : null;
		ps.setObject(paramInt, paramTypeId);
	}

	@Override
	public PayType getResult(ResultSet rs, int columnIndex) throws SQLException {
		return PayType.getById(rs.getInt(columnIndex));
	}
}
