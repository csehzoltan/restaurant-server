package org.restaurant.server.data.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;
import org.restaurant.common.model.beans.Unit;

@MappedTypes(Unit.class)
public class UnitTypeHandler implements TypeHandler<Unit> {

	@Override
	public Unit getResult(ResultSet rs, String param) throws SQLException {
		return Unit.getById(rs.getInt(param));
	}

	@Override
	public Unit getResult(CallableStatement cs, int col) throws SQLException {
		return Unit.getById(cs.getInt(col));
	}

	@Override
	public void setParameter(PreparedStatement ps, int paramInt, Unit paramType, JdbcType jdbctype)
			throws SQLException {
		ps.setInt(paramInt, paramType.getId());
	}

	@Override
	public Unit getResult(ResultSet rs, int columnIndex) throws SQLException {
		return Unit.getById(rs.getInt(columnIndex));
	}
}
