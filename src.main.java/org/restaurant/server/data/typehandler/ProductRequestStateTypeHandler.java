package org.restaurant.server.data.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;
import org.restaurant.common.model.beans.ProductRequestState;

	@MappedTypes(ProductRequestState.class)
	public class ProductRequestStateTypeHandler implements TypeHandler<ProductRequestState> {

		@Override
		public ProductRequestState getResult(ResultSet rs, String param) throws SQLException {
			return ProductRequestState.getById(rs.getInt(param));
		}

		@Override
		public ProductRequestState getResult(CallableStatement cs, int col) throws SQLException {
			return ProductRequestState.getById(cs.getInt(col));
		}

		@Override
		public void setParameter(PreparedStatement ps, int paramInt, ProductRequestState paramType, JdbcType jdbctype)
				throws SQLException {
			ps.setInt(paramInt, paramType.getId());
		}

		@Override
		public ProductRequestState getResult(ResultSet rs, int columnIndex) throws SQLException {
			return ProductRequestState.getById(rs.getInt(columnIndex));
		}
}
