package org.restaurant.server.data.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;
import org.restaurant.common.model.beans.ProductCategory;

@MappedTypes(ProductCategory.class)
public class ProductCategoryTypeHandler  implements TypeHandler<ProductCategory> {

	@Override
	public ProductCategory getResult(ResultSet rs, String param) throws SQLException {
		return ProductCategory.getById(rs.getInt(param));
	}

	@Override
	public ProductCategory getResult(CallableStatement cs, int col) throws SQLException {
		return ProductCategory.getById(cs.getInt(col));
	}

	@Override
	public void setParameter(PreparedStatement ps, int paramInt, ProductCategory paramType, JdbcType jdbctype)
			throws SQLException {
		ps.setInt(paramInt, paramType.getId());
	}

	@Override
	public ProductCategory getResult(ResultSet rs, int columnIndex) throws SQLException {
		return ProductCategory.getById(rs.getInt(columnIndex));
	}
}
