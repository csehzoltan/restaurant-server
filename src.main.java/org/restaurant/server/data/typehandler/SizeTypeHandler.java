package org.restaurant.server.data.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;
import org.restaurant.common.model.beans.Size;

@MappedTypes(Size.class)
public class SizeTypeHandler implements TypeHandler<Size> {

	@Override
	public Size getResult(ResultSet rs, String param) throws SQLException {
		return Size.getById(rs.getInt(param));
	}

	@Override
	public Size getResult(CallableStatement cs, int col) throws SQLException {
		return Size.getById(cs.getInt(col));
	}

	@Override
	public void setParameter(PreparedStatement ps, int paramInt, Size paramType, JdbcType jdbctype)
			throws SQLException {
		ps.setInt(paramInt, paramType.getId());
	}

	@Override
	public Size getResult(ResultSet rs, int columnIndex) throws SQLException {
		return Size.getById(rs.getInt(columnIndex));
	}
}
