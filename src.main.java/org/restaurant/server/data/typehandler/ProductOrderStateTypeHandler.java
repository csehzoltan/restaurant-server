package org.restaurant.server.data.typehandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;
import org.restaurant.common.model.beans.ProductOrderState;

@MappedTypes(ProductOrderState.class)
public class ProductOrderStateTypeHandler implements TypeHandler<ProductOrderState> {

	@Override
	public ProductOrderState getResult(ResultSet rs, String param) throws SQLException {
		return ProductOrderState.getById(rs.getInt(param));
	}

	@Override
	public ProductOrderState getResult(CallableStatement cs, int col) throws SQLException {
		return ProductOrderState.getById(cs.getInt(col));
	}

	@Override
	public void setParameter(PreparedStatement ps, int paramInt, ProductOrderState paramType, JdbcType jdbctype)
			throws SQLException {
		ps.setInt(paramInt, paramType.getId());
	}

	@Override
	public ProductOrderState getResult(ResultSet rs, int columnIndex) throws SQLException {
		return ProductOrderState.getById(rs.getInt(columnIndex));
	}
}
