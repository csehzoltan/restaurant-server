package org.restaurant.server.properties;

public class PropertyException extends Exception {
	private static final long serialVersionUID = 1L;

	public PropertyException(String string) {
		super(string);
	}

	public PropertyException(String string, Throwable e) {
		super(string, e);
	}

}
