package org.restaurant.server.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ServerConfig {
	
	private Integer port;
	private boolean isLocalhostDB;

	/**
	 * Konfigurációs fájlból beolvasott adatokat kezeli, tárolja, melyek a következők lehetnek: port
	 * @throws PropertyException
	 */
	public ServerConfig() throws PropertyException {
		process();
	}
	
	private void process() throws PropertyException {
		Properties prop = new Properties();

		try (FileInputStream is = new FileInputStream(new File(System.getenv("APPDATA") + "/thesis/server/resources/config.properties"))) {
			prop.load(is);
			
			port = prop.getProperty("port") != null ? Integer.parseInt(prop.getProperty("port")) : null;
			isLocalhostDB = Boolean.valueOf(prop.getProperty("isLocalhostDB"));
		} catch (IOException e) {
			throw new PropertyException("Error in loading config properties...", e);			
		}
	}
	
	/**
	 * Visszatér a port számmal.
	 * @return port szám
	 */
	public Integer getPort() {
		return port;
	}

	public boolean isLocalhostDB() {
		return isLocalhostDB;
	}
	
}
