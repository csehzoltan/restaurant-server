package org.restaurant.server.action;

import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.Map;

import org.restaurant.common.json.JsonHelper;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

public class AbstractAction {
	
	protected static final String ACTION = "action";
	protected static final String ERROR = "error";
	protected static final String DATA = "data";
	protected static final String INFO = "info";
	
	protected JsonElement rawData;
	protected PrintWriter pw;
	
	public AbstractAction(JsonElement data, PrintWriter pw) {
		super();
		this.rawData = data;
		this.pw = pw;
	}
	
	protected Map<String, String> getInfos() {
		Gson gson = new Gson();
		Type typeOfHashMap = new TypeToken<Map<String, String>>() { }.getType();
		Map<String, String> infos = null;
		if(rawData.getAsJsonObject().get("info") != null) {
			infos = gson.fromJson(rawData.getAsJsonObject().get("info"), typeOfHashMap);
		}
		return infos;
	}
	
	/**
	 * A megadott paraméterekből Json üzenetet állít elő.
	 * @param action művelet neve.
	 * @param data művelethez tartozó objektum.
	 * @param info információs üzeneteket tartalmazó map.
	 */
	protected String getJsonMsg(String action, Object data, Map<String, String> info) {
		return JsonHelper.createJsonMsg(action, data, info);
	}
	
	/**
	 * A megadott paraméterekből Json üzenetet állít elő és elküldi a kliensnek.
	 * @param action művelet neve.
	 * @param data művelethez tartozó objektum.
	 * @param info információs üzeneteket tartalmazó map.
	 * @param flush egyből küldődjön az üzenet.
	 */
	protected void sendJsonMsg(String action, Object data, Map<String, String> info, boolean flush) {
		pw.println(JsonHelper.createJsonMsg(action, data, info));
		if(flush) {
			pw.flush();
		}
	}
	
	protected JsonElement getAction() {
		return rawData.getAsJsonObject().get(ACTION);
	}
	
	protected JsonElement getError() {
		return rawData.getAsJsonObject().get(ERROR);
	}
	
	protected JsonElement getData() {
		return rawData.getAsJsonObject().get(DATA);
	}
	
	protected JsonElement getInfo() {
		return rawData.getAsJsonObject().get(INFO);
	}
	
	protected JsonObject getAsJsonObject(String name) {
		return rawData.getAsJsonObject().getAsJsonObject(name);
	}
	
	protected void sendErrorMsg(JsonObject sendMsg, String actionName, String msg) {
		if(sendMsg == null) {
			sendMsg = new JsonObject();
		}
		sendMsg.addProperty(ACTION, actionName);
		sendMsg.addProperty(ERROR, msg);
		pw.println(sendMsg.toString());
		pw.flush();
	}
	
}
