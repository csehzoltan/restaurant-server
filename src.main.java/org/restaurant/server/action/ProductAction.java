package org.restaurant.server.action;

import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.restaurant.common.model.beans.CoordXY;
import org.restaurant.common.model.beans.Ingredient;
import org.restaurant.common.model.beans.Product;
import org.restaurant.common.model.beans.ProductIngredient;
import org.restaurant.common.model.beans.ProductPrice;
import org.restaurant.common.model.beans.UserType;
import org.restaurant.server.data.controller.IngredientController;
import org.restaurant.server.data.controller.ProductController;
import org.restaurant.server.data.controller.ProductPriceController;
import org.restaurant.server.data.mapper.MyBatisFactory;
import org.restaurant.server.data.mapper.ProductMapper;
import org.restaurant.server.exception.controller.ControllerException;
import org.restaurant.server.exception.controller.ExistObjectException;
import org.restaurant.server.exception.controller.MissingBeanControllerException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

public class ProductAction extends AbstractAction {

	public ProductAction(JsonElement data, PrintWriter pw) {
		super(data, pw);
	}

	public void getProducts() {
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			ProductMapper productMapper = session.getMapper(ProductMapper.class);
			ProductController controller = new ProductController(productMapper);
			
			Integer userTypeId = rawData.getAsJsonObject().get(DATA).getAsInt();
			
			Map<String, Object> productFilter = new HashMap<>();
			productFilter.put("showOnlyNotDeleted", UserType.GUEST.getId() == userTypeId);
			
			JsonObject sendMsg = createJsonMsg(controller.getProducts(productFilter));
			pw.println(sendMsg.toString());
			pw.flush();
		}
	}
	
	/**
	 * Termék törlése az adatbázisból, deleted(true) későbbi fejlesztéseknél logikai tölréshez, azonban most még teljes törlés történik.
	 */
	public void deleteProduct() {
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			Integer productId = rawData.getAsJsonObject().get("data").getAsInt();
			
			ProductMapper productMapper = session.getMapper(ProductMapper.class);
			ProductController productController = new ProductController(productMapper, productId);
			productController.delete(productController.getBean());
			session.commit();
		} catch (MissingBeanControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void getIngredients() {
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			ProductMapper productMapper = session.getMapper(ProductMapper.class);
			IngredientController controller = new IngredientController(productMapper);
			pw.println(createJsonMsg("ingredients", controller.getIngredients(), new TypeToken<List<Ingredient>>() {}.getType()).toString());
			pw.flush();
		}
	}
	
	public void setIngredientAlerts() {
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			ProductMapper productMapper = session.getMapper(ProductMapper.class);
			IngredientController ingredientController = new IngredientController(productMapper);
			Map<String, Object> filter = new HashMap<>();
			filter.put("getUnderLimit", "yes");
			List<Ingredient> ingredients = ingredientController.getIngredientsFiltered(filter);
			if(ingredients != null) {
				for(int i = 0; i < ingredients.size(); i++) {
					ingredients.get(i).setIngredientImg(null);
				}
			}
			
			JsonObject sendMsg = createJsonMsgList(ingredients);	
			pw.println(sendMsg.toString());
			pw.flush();
		}
	}
	
	public void getProductCategoryStatistics() {
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			String searchDate = rawData.getAsJsonObject().get(DATA) != null ? rawData.getAsJsonObject().get(DATA).getAsString() : null;
			ProductMapper productMapper = session.getMapper(ProductMapper.class);
			
			Map<String, Object> res = new HashMap<>();
			res.put("year", searchDate != null ? searchDate.substring(0, 4) : null);
			res.put("month", searchDate != null ? searchDate.substring(4, 6) : null);
			List<CoordXY> incomes = productMapper.selectProductCategoryStatistics(res);
			pw.println(createJsonMsg(DATA, incomes, new TypeToken<List<CoordXY>>() {}.getType()).toString());
			pw.flush();
		} catch (StringIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void getProductIncomes() {
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			Integer year = rawData.getAsJsonObject().get(DATA) != null ? Integer.parseInt(rawData.getAsJsonObject().get(DATA).getAsString()) : null;
			ProductMapper productMapper = session.getMapper(ProductMapper.class);
			Map<String, Object> res = new HashMap<>();
			res.put("year", year);
			List<CoordXY> incomes = productMapper.selectProductStatisticsIncomes(res);
			pw.println(createJsonMsg(DATA, incomes, new TypeToken<List<CoordXY>>() {}.getType()).toString());
			pw.flush();
		}
	}
	
	public void uploadIngredients() {
		try (SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			Type listType = new TypeToken<ArrayList<Ingredient>>() {}.getType();
			List<Ingredient> ingredients = new Gson().fromJson(rawData.getAsJsonObject().getAsJsonArray("data"), listType);

			try {
				List<Ingredient> noIdIngredients = new ArrayList<>();
				for (Ingredient ingredient : ingredients) {
					ProductMapper productMapper = session.getMapper(ProductMapper.class);
					IngredientController controller = new IngredientController(productMapper, ingredient);
					if(!ingredient.isDeleted()) {
						if(ingredient.getId() == null) {
							noIdIngredients.add(ingredient);
						}
						controller.save();
					} else {
						controller.delete(ingredient);
					}
				}
				session.commit();
				
				pw.println(createJsonMsg("ingredients", ingredients, new TypeToken<List<Ingredient>>() {}.getType()).toString());
				pw.flush();
			} catch (ControllerException e) {
				session.rollback();
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Termék mentése adminisztrátor által, eldönti, hogy beszúró vagy módosító műveletet hajtunk végre.
	 * Végül válaszol az adminisztrátornak a mentés sikerességéről és visszaküldi a terméket (id miatt).
	 */
	public void saveProductByAdmin() {	
		Gson gson = new Gson();
		Product product = gson.fromJson(getAsJsonObject(DATA), Product.class);
		Integer gottenProductId = product.getId();
		JsonObject responseMsg = new JsonObject();
		
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			
			ProductMapper productMapper = session.getMapper(ProductMapper.class);
			ProductController productController = new ProductController(productMapper, product);
			productController.save();

			saveProductPrices(productMapper, productController);

			List<Ingredient> origIngredients = getOrigIngredientIds(product, productMapper);
			
			//delete unused ingredients
			if(origIngredients != null) {
				for(Ingredient origIngredient : origIngredients) {
					if(!productController.getBean().getIngredients().contains(origIngredient)) {
						productMapper.deleteProductIngredientByProdAndIngredId(product.getId(), origIngredient.getId());
					}
				}
			}
			//insert, update prod ingredient
			if(productController.getBean().getIngredients() != null) {
				for(Ingredient ingredient : productController.getBean().getIngredients()) {
					Integer unitId = ingredient.getUnit() != null ? ingredient.getId() : null;
					ProductIngredient prodIngredient = new ProductIngredient(product.getId(), ingredient.getId(), ingredient.getQuantity(), unitId);
					if(origIngredients != null && origIngredients.contains(ingredient)) {
						productMapper.updateProductIngredientByProdAndIngredientId(prodIngredient);
					} else {
						productMapper.insertProductIngredient(prodIngredient);
					}
				}
			}
			session.commit();

			if(productController.getBean() != null) {
				Map<String, String> infos = new HashMap<>();
				infos.put("type", gottenProductId == null ? "insert" : "update");
				sendJsonMsg("saveProductByAdmin", productController.getBean(), infos, true);
			}

		} catch (ExistObjectException e) {
			responseMsg.addProperty(ACTION, "saveProductByAdmin");
			responseMsg.addProperty(ERROR, "Exist product name! Please choose an other one!");
			pw.println(responseMsg.toString());
			pw.flush();
		} catch (ControllerException e) {
			sendErrorMsg(responseMsg, "saveProductByAdmin", "Internal error! Contact the staff!");
		}

	}

	private void saveProductPrices(ProductMapper productMapper, ProductController productController)
			throws ControllerException {
		if(productController.getBean().getPrices() != null) {
			for(ProductPrice price : productController.getBean().getPrices()) {
				price.setProduct(productController.getBean());
				ProductPriceController priceController = new ProductPriceController(productMapper, price);
				priceController.save();
				//Válasz miatt törlöm a terméket, duplikáció átküldés miatt
				priceController.getBean().setProduct(null);
			}
		}
	}

	private List<Ingredient> getOrigIngredientIds(Product product, ProductMapper productMapper) {
		List<Ingredient> originProdIngredients = null;
		if(product.getId() != null) {
			return productMapper.selectProductIngredients(product.getId());	
		}
		return originProdIngredients;
	}
	
	
	//TODO common method
	private JsonObject createJsonMsg(String objName, Object src, Type typeSrc) {
		JsonObject sendMsg = new JsonObject();
		Gson gson = new GsonBuilder().create();
		JsonElement obj = gson.toJsonTree(src, typeSrc);
		sendMsg.add("action", getAction());
		sendMsg.add(objName, obj);
		return sendMsg;
	}

	// TODO generics can be use
	private JsonObject createJsonMsg(List<Product> products) {
//		for(Product prod : products) {
//			prod.setImg(null);
//		}
		JsonObject sendMsg = new JsonObject();
		try {
			Gson gson = new GsonBuilder().create();
			JsonElement obj = gson.toJsonTree(products, new TypeToken<List<Product>>() {}.getType());

			sendMsg.add("action", getAction());
			sendMsg.add("products", obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sendMsg;
	}
	
	private JsonObject createJsonMsgList(List<Ingredient> ingredients) {
		JsonObject sendMsg = new JsonObject();
		try {
			Gson gson = new GsonBuilder().create();
			JsonElement obj = gson.toJsonTree(ingredients, new TypeToken<List<Ingredient>>() {}.getType());

			sendMsg.add(ACTION, getAction());
			sendMsg.add(DATA, obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sendMsg;
	}
}
