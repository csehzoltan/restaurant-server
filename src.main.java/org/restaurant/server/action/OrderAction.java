package org.restaurant.server.action;

import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.ibatis.session.SqlSession;
import org.restaurant.common.model.beans.Chair;
import org.restaurant.common.model.beans.Ingredient;
import org.restaurant.common.model.beans.PayType;
import org.restaurant.common.model.beans.ProductOrder;
import org.restaurant.common.model.beans.ProductRequest;
import org.restaurant.common.model.beans.ProductRequestState;
import org.restaurant.server.data.controller.IngredientController;
import org.restaurant.server.data.controller.ProductOrderController;
import org.restaurant.server.data.controller.ProductRequestController;
import org.restaurant.server.data.mapper.CommonMapper;
import org.restaurant.server.data.mapper.MyBatisFactory;
import org.restaurant.server.data.mapper.ProductMapper;
import org.restaurant.server.exception.controller.ControllerException;
import org.restaurant.server.exception.controller.MissingBeanControllerException;
import org.restaurant.server.socket.RestaurantServer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

public class OrderAction extends AbstractAction {
	
	public OrderAction(JsonElement data, PrintWriter pw) {
		super(data, pw);
	}

	/**
	 * Feladata a bejövő rendelések feldolgozása. Kapott Json egyik eleme a data, ami tartalmazza a
	 * rendeléseket egy mapben, melynek kulcsa a szék neve és a production price id, értéke pedig a rendelt mennyiség.
	 * Beszúrja az adatbázisba az adatokat és kiosztja egy felszolgálónak a rendelést. Az info json objektum az asztal id-ját tartalmazza.
	 */
	protected void getOrder() {
		JsonObject orderData = rawData.getAsJsonObject().getAsJsonObject("data");
		Integer userId = rawData.getAsJsonObject().get("userId") == null ? null : rawData.getAsJsonObject().get("userId").getAsInt();
		Integer tableId = rawData.getAsJsonObject().get("info").getAsInt();
		JsonObject responseMsg = new JsonObject();
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			ProductMapper productMapper = session.getMapper(ProductMapper.class);
			CommonMapper commonMapper = session.getMapper(CommonMapper.class);
			
			ProductRequestController requestController = new ProductRequestController(productMapper);
			
			Map<String, Object> tableAndStatusFilter = new HashMap<>();
			tableAndStatusFilter.put("tableId", String.valueOf(tableId));
			tableAndStatusFilter.put("stateId", String.valueOf(ProductRequestState.PAYING.getId()));
			List<ProductRequest> payingRequestForTable = requestController.getDao().getMapper().selectProductRequestByConditions(tableAndStatusFilter);
			if(payingRequestForTable != null && !payingRequestForTable.isEmpty()) {
				sendThereIsAnActivePayingMsg();
			} else {
				Integer selectedWaiterId = null;
				List<Integer> loggedInWaiters = RestaurantServer.getWaiters() == null ? null
						: RestaurantServer.getWaiters().keySet().stream().collect(Collectors.toList());

				if (loggedInWaiters == null || loggedInWaiters.isEmpty()) {
					requestController.createNewRequest(userId, null, tableId, true);
					requestController.save();
				} else {
					selectedWaiterId = requestController.createRequestToWaiter(tableId, userId, loggedInWaiters);
					requestController.save();
				}

				createOrders(orderData, tableId, productMapper, commonMapper, requestController.getBean().getId());

				if (selectedWaiterId != null) {
					sendRequestToWaiter(productMapper, requestController, selectedWaiterId);
				}

				session.commit();
				responseMsg = createJsonMsg("success", "acceptOrder", DATA);
				responseMsg.addProperty(INFO, "Order accepted!");
				pw.println(responseMsg.toString());
				pw.flush();
			}
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			responseMsg.addProperty(ACTION, "acceptOrder");
			responseMsg.addProperty(ERROR, "Unsuccessful ordering!");
			pw.println(responseMsg.toString());
			pw.flush();
			e.printStackTrace();
		}
	}

	private void sendThereIsAnActivePayingMsg() {
		JsonObject responseMsg;
		responseMsg = createJsonMsg("error", "acceptOrder", DATA);
		responseMsg.addProperty(ERROR, "Pleas try again later! There is an active paying for this table!");
		responseMsg.addProperty(INFO, "Pleas try again later! There is an active paying for this table!");
		pw.println(responseMsg.toString());
		pw.flush();
	}

	private void sendRequestToWaiter(ProductMapper productMapper, ProductRequestController requestController,
			Integer selectedWaiterId) throws MissingBeanControllerException {
		requestController = new ProductRequestController(productMapper, requestController.getBean().getId());
		
		ProductRequest waiterRequest = productMapper.selectProductRequestTest(requestController.getBean().getId());
		PrintWriter waiterPrintWriter = RestaurantServer.getWaiters().get(selectedWaiterId);
		waiterPrintWriter.println(createJsonMsg(waiterRequest, "acceptNewOrder", "productRequest").toString());
		waiterPrintWriter.flush();
	}

	private void createOrders(JsonObject orderData, Integer tableId, ProductMapper productMapper,
			CommonMapper commonMapper, Integer productRequestId) throws ControllerException {
		Gson gson = new Gson();
		Type type = new TypeToken<Map<String, Integer>>(){}.getType();
		Map<String, Integer> orderMap = gson.fromJson(orderData, type);
		for(Map.Entry<String, Integer> entry : orderMap.entrySet()) {
			Chair chair = commonMapper.selectChairByTableIdAndChairName(tableId, entry.getKey().split("_")[0]);
			Integer schairId = chair == null ? null : chair.getId();
			Integer productPriceId = Integer.valueOf(entry.getKey().split("_")[1]);

			ProductOrderController orderController = new ProductOrderController(productMapper);
			orderController.createNewProductOrder(productRequestId, productPriceId, entry.getValue(), 0, schairId);
			orderController.save();
		}
	}
	
	protected void payGuest() {

		try (SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			ProductMapper productMapper = session.getMapper(ProductMapper.class);
			ProductRequestController prodReqCtrl = new ProductRequestController(productMapper);

			Map<String, Object> tableAndStatusFilter = new HashMap<>();
			tableAndStatusFilter.put("tableId", String.valueOf(getInfos().get("tableId")));
			tableAndStatusFilter.put("stateId", String.valueOf(ProductRequestState.PAYING.getId()));
			List<ProductRequest> payingRequestForTable = prodReqCtrl.getDao().getMapper()
					.selectProductRequestByConditions(tableAndStatusFilter);
			if (payingRequestForTable != null && !payingRequestForTable.isEmpty()) {
				sendThereIsAnActivePayingMsg();
			} else {
				Map<String, Object> reqFilter = new HashMap<>();
				reqFilter.put("tableId", getInfos().get("tableId"));
				reqFilter.put("stateId", ProductRequestState.OPEN.getId());
				List<ProductRequest> request = prodReqCtrl.getProductRequestsByFilter(reqFilter);

				if (request != null && !request.isEmpty()) {
					prodReqCtrl.setBean(request.get(0));
					prodReqCtrl.paying(PayType.getById(Integer.parseInt(getInfos().get("payTypeId"))));
					prodReqCtrl.save();

					// should be thread safe map
					Map<Integer, PrintWriter> logedInWaiters = RestaurantServer.getWaiters();
					if (logedInWaiters != null && !logedInWaiters.isEmpty()) {
						PrintWriter waiterPw = logedInWaiters.get(prodReqCtrl.getBean().getWaiter().getId());
						// ellenőrizni az ordernél, hogy null-e
						if (waiterPw != null) {
							// sendben nem jó hogy a pw benne van.
							waiterPw.println(getJsonMsg("getPayForWaiter", prodReqCtrl.getBean(), null));
							waiterPw.flush();
							// //a túloldalon ha megvan nyitva szerkesztésre a request, akkor paying
							// státuszt ne veszítse el, thread safe legyen
						}
					}
				}

				Map<String, String> reqInfo = new HashMap<>();
				reqInfo.put(INFO, request != null ? "Successfully sent the payment intent" : "No opened order found!");
				pw.println(getJsonMsg("payGuest", null, reqInfo));
				pw.flush();

				session.commit();
			}

		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Elmenti az adatbázisba a felszolgálók által módosított rendeléseket.
	 */
	protected void saveRequestOrdersByWaiter() {

		List<ProductOrder> modifiedProductOrders = new ArrayList<>();
		if (rawData.getAsJsonObject().get(DATA) != null) {
			Gson gson = new Gson();
			Type typeOfHashMap = new TypeToken<ArrayList<ProductOrder>>() {}.getType();
			modifiedProductOrders = gson.fromJson(rawData.getAsJsonObject().get(DATA), typeOfHashMap);

			try (SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
				ProductMapper productMapper = session.getMapper(ProductMapper.class);
				for (ProductOrder order : modifiedProductOrders) {
					ProductOrderController orderController = new ProductOrderController(productMapper, order);
					orderController.save();
				}
				session.commit();
			} catch (ControllerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	protected void acceptPayByWaiter() {
		Integer requestId = rawData.getAsJsonObject().get("data").getAsInt();
		
		JsonObject responseMsg = new JsonObject();
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			ProductMapper productMapper = session.getMapper(ProductMapper.class);
			ProductRequestController requestController = new ProductRequestController(productMapper, requestId);
			
			ProductRequest productRequest = productMapper.selectProductRequestTest(requestId);
			
			deleteIngredients(productMapper, productRequest);
			
			requestController.getBean().setState(ProductRequestState.CLOSED);
			requestController.save();
			session.commit();
			
			responseMsg = createJsonMsg("success", "acceptPayByWaiter", "data");
	    	pw.println(responseMsg.toString());
	    	pw.flush();
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			responseMsg.addProperty(ACTION, "acceptPayByWaiter");
			responseMsg.addProperty(ERROR, "Unsuccessful pay!");
			pw.println(responseMsg.toString());
			pw.flush();
			e.printStackTrace();
		}
	}

	private void deleteIngredients(ProductMapper productMapper, ProductRequest productRequest)
			throws MissingBeanControllerException, ControllerException {
		for(ProductOrder order : productRequest.getProductOrders()) {
			Integer quantity = order.getQuantity();
			if(order.getProductPrice().getProduct().getIngredients() != null && !order.getProductPrice().getProduct().getIngredients().isEmpty()) {
				for(Ingredient ingredient : order.getProductPrice().getProduct().getIngredients()) {
					IngredientController ingredientController = new IngredientController(productMapper, ingredient.getId());
					Double ingredientQuantity = ingredientController.getBean().getQuantity() == null ? ingredientController.getBean().getQuantity() : 0.0;
					Double minusQuantity = ingredientQuantity - (quantity * ingredient.getQuantity());
				
					if((ingredientQuantity - minusQuantity) < 0) {
						ingredientQuantity = 0.0;
					} else {
						ingredientQuantity = ingredientQuantity - minusQuantity;
					}
				
					ingredientController.save();
				}
			}
		}
	}
	
	private JsonObject createJsonMsg(Object data, String actionName, String paramName) {
		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(data);
		JsonElement obj = new JsonParser().parse(json);

		JsonElement actionNameJsonElement = new JsonParser().parse(actionName);
		JsonObject sendMsg = new JsonObject();

		sendMsg.add("action", actionNameJsonElement);
		sendMsg.add(paramName, obj);
		return sendMsg;
	}
}

