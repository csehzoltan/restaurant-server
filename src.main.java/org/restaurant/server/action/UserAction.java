package org.restaurant.server.action;

import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.restaurant.common.json.JsonHelper;
import org.restaurant.common.model.beans.Guestbook;
import org.restaurant.common.model.beans.ProductRequest;
import org.restaurant.common.model.beans.User;
import org.restaurant.common.model.beans.UserType;
import org.restaurant.server.data.controller.GuestbookController;
import org.restaurant.server.data.controller.ProductRequestController;
import org.restaurant.server.data.controller.UserController;
import org.restaurant.server.data.mapper.MyBatisFactory;
import org.restaurant.server.data.mapper.ProductMapper;
import org.restaurant.server.data.mapper.UserMapper;
import org.restaurant.server.error.Errors;
import org.restaurant.server.exception.controller.ControllerException;
import org.restaurant.server.exception.controller.ExistObjectException;
import org.restaurant.server.exception.controller.LongDataException;
import org.restaurant.server.exception.controller.MissingBeanControllerException;
import org.restaurant.server.socket.RestaurantServer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

public class UserAction extends AbstractAction {

	public UserAction(JsonElement data, PrintWriter pw) {
		super(data, pw);
	}

	public void registration() {
		JsonObject userObj = rawData.getAsJsonObject().getAsJsonObject("data");
		Gson gson = new Gson();
		User user = gson.fromJson(userObj, User.class);
		Date date = null;
		
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			
			String dateOfBirth = getInfos() != null ? getInfos().get("dateOfBirth") : null;
			DateFormat format = new SimpleDateFormat("yyyymmdd", Locale.ENGLISH);
			date = format.parse(dateOfBirth);
			user.setBirthDay(date);
			user.setRegistrationDate(new Date());
			
			UserMapper userMapper = session.getMapper(UserMapper.class);
			UserController userController = new UserController(userMapper, user);
			userController.save();
			session.commit();
			
			pw.println(getJsonMsg("registration", userController.getBean(), null));
			pw.flush();
		}  catch (ExistObjectException e) {
			sendErrorMsg(null, "registration", "Username already exists! Please choose an other username!");
		} catch (ControllerException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void closeWaiter(Map<Integer, PrintWriter> waiters) {
		Integer waiterId = Integer.valueOf(getInfos().get("waiterId"));
		if(waiters != null) {
			waiters.remove(waiterId);
		}
	}
	
	/**
	 * Felhasználó mentése adminisztrátor által, eldönti, hogy beszúró vagy módosító műveletet hajtunk végre.
	 * Végül válaszol az adminisztrátornak a mentés sikerességéről és visszaküldi a felhasználót (update id miatt).
	 */
	public void registrationByAdmin() {	
		Gson gson = new Gson();
		User user = gson.fromJson(getAsJsonObject(DATA), User.class);
		Integer gottenUserId = user.getId();
		JsonObject responseMsg = new JsonObject();
		
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			UserMapper userMapper = session.getMapper(UserMapper.class);
			if(user.getId() == null) {
				user.setRegistrationDate(new Date());
			} else {
				UserController userControllerOrig = new UserController(userMapper, user.getId());
				user.setRegistrationDate(userControllerOrig.getBean() != null ? userControllerOrig.getBean().getRegistrationDate() : null);
			}
			UserController userController = new UserController(userMapper, user);
			userController.save();
			session.commit();

			if(userController.getBean() != null) {
				Map<String, String> infos = new HashMap<>();
				infos.put("type", gottenUserId == null ? "insert" : "update");
				sendJsonMsg("registrationByAdmin", userController.getBean(), infos, true);
			}

		} catch (ExistObjectException e) {
			responseMsg.addProperty(ACTION, "registrationByAdmin");
			responseMsg.addProperty(ERROR, "Username already exists! Please choose an other username!");
			pw.println(responseMsg.toString());
			pw.flush();
		} catch (ControllerException e) {
			sendErrorMsg(responseMsg, "registrationByAdmin", Errors.ERROR_GENERAL_ERROR.getDisplay());
		}

	}
	
	public void getUsers() {
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			UserMapper userMapper = session.getMapper(UserMapper.class);
			UserController userController = new UserController(userMapper);
			JsonObject sendMsg = createJsonMsg(userController.getUsers());
		    pw.println(sendMsg.toString());
			pw.flush();
		}
	}
	
	protected void saveGuestbook() {
		JsonObject jsonGuestbook = rawData.getAsJsonObject().getAsJsonObject("data");
		Gson gson = new Gson();
		Guestbook guestbook = gson.fromJson(jsonGuestbook, Guestbook.class);
		
		JsonObject responseMsg = new JsonObject();
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			UserMapper userMapper = session.getMapper(UserMapper.class);
			GuestbookController guestbookController = new GuestbookController(userMapper, guestbook);
			guestbookController.getBean().setArrivedAt(new Date());
			guestbookController.save();
			session.commit();
			responseMsg.addProperty(ACTION, "saveGuestbook");
		} catch (LongDataException e) {
			responseMsg.addProperty(ACTION, "saveGuestbook");
			responseMsg.addProperty(ERROR, e.getMessage());
		} catch (ControllerException e) {
			responseMsg.addProperty(ACTION, "saveGuestbook");
			responseMsg.addProperty(ERROR, "Saving your guestbook failed! Please try again.");
		}
		
		pw.println(responseMsg.toString());
		pw.flush();
	}
	
	public void getGuestbooks() {
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			UserMapper userMapper = session.getMapper(UserMapper.class);
			GuestbookController guestbookController = new GuestbookController(userMapper);
			
			List<Guestbook> guestBooks = guestbookController.getGuestbooks();
			for(int i = 0; i < guestBooks.size(); i++) {
				User user = guestBooks.get(i).getUser();
				if(user != null) {
					UserController userController = new UserController(userMapper, user.getId());
					guestBooks.get(i).setUser(userController.getBean());
				}
			}
			
			JsonObject sendMsg = createJsonMsgList(guestBooks);	
			pw.println(sendMsg.toString());
			pw.flush();
		} catch (MissingBeanControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void getGuestBookAdministrator() {
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			String searchDate = rawData.getAsJsonObject().get(DATA) != null ? rawData.getAsJsonObject().get(DATA).getAsString() : null;
			
			Map<String, Object> res = new HashMap<>();
			res.put("year", searchDate != null ? searchDate.substring(0, 4) : null);
			res.put("month", searchDate != null ? searchDate.substring(4, 6) : null);
			UserMapper userMapper = session.getMapper(UserMapper.class);
			
			List<Guestbook> guestBooks = userMapper.selectGuestBookAdministrator(res);

			for(int i = 0; i < guestBooks.size(); i++) {
				User user = guestBooks.get(i).getUser();
				if(user != null) {
					UserController userController = new UserController(userMapper, user.getId());
					guestBooks.get(i).setUser(userController.getBean());
				}
			}
			
			JsonObject sendMsg = createJsonMsgList(guestBooks);	
			pw.println(sendMsg.toString());
			pw.flush();
		} catch (StringIndexOutOfBoundsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MissingBeanControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Felhasználót bejelentkezteti, ellenőrzi a felhasználói adatokat felhasználónév, jelszó, típus szerint, amennyiben sikerül a bejelentkezés 
	 * visszajelzi a kliensnek a sikeres bejelentkezést és a felhasználói adatokat, pincér esetén a hozzá tartozó megrendeléseket, amennyiben van olyan 
	 * megrendelés amihez még nincs felszolgáló csatolva, akkor hozzá fogja rakni.
	 */
	public void logIn() {
		JsonObject userObj = rawData.getAsJsonObject().getAsJsonObject("data");
		Gson gson = new Gson();
		User user = gson.fromJson(userObj, User.class);
		
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			UserMapper userMapper = session.getMapper(UserMapper.class);
			ProductMapper productMapper = session.getMapper(ProductMapper.class);
			UserController userController = new UserController(userMapper);
			Map<String, Object> filter = new HashMap<>();
			filter.put("loginCheck", true);
			userController.getUserByLoginData(user, filter);
			
			JsonObject sendMsg = new JsonObject();
			if(userController.getBean() != null){
				if(userController.getBean().getType().equals(UserType.WAITER)) {
					Map<String, Object> requestFilter = new HashMap<>();
					requestFilter.put("getNoWaiterRequests", true);
					ProductRequestController prodReqCtrl = new ProductRequestController(productMapper);
					List<ProductRequest> noWaiterRequests = prodReqCtrl.getProductRequestsByFilter(requestFilter);
					if(noWaiterRequests != null && !noWaiterRequests.isEmpty()) {
						for(ProductRequest request : noWaiterRequests) {
							User waiterWithNoRequests = new User();
							waiterWithNoRequests.setId(userController.getBean().getId());
							request.setWaiter(waiterWithNoRequests);
							userController.getBean().getProductRequests().add(request);
							prodReqCtrl.setBean(request);
							prodReqCtrl.save();
						}
					}
					
					RestaurantServer.getWaiters().put(userController.getBean().getId(), pw);
				}
				sendMsg = createUserJsonMsg("logIn", userController.getBean());
		    	pw.println(sendMsg.toString());
			} else {
				sendMsg.addProperty(ACTION, "logIn");
				sendMsg.addProperty(ERROR, "Wrong user or password!");
				pw.println(sendMsg.toString());
			}
			pw.flush();
			session.commit();
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}
	
	private JsonObject createUserJsonMsg(String actionType, User user) {
			
			Gson gson = new GsonBuilder().create();
	        String json = gson.toJson(user);
	        JsonObject obj = new JsonParser().parse(json).getAsJsonObject();
	        
	        JsonElement actionNameJsonElement=new JsonParser().parse(actionType);
			JsonObject sendMsg = new JsonObject();
			
	        sendMsg.add(ACTION, actionNameJsonElement);
	        sendMsg.add(DATA, obj);
			return sendMsg;
	}
	
	
	private JsonObject createJsonMsgList(List<Guestbook> guestbooks) {
		JsonObject sendMsg = new JsonObject();
		try {
			Gson gson = new GsonBuilder().create();
			JsonElement obj = gson.toJsonTree(guestbooks, new TypeToken<List<Guestbook>>() {}.getType());

			sendMsg.add(ACTION, getAction());
			sendMsg.add(DATA, obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sendMsg;
	}
	
	// TODO json helpert írni és átalánosítani bármilyen lista objektumra.
	private JsonObject createJsonMsg(List<User> users) {
		JsonObject sendMsg = new JsonObject();
		try {
			Gson gson = new GsonBuilder().create();
			JsonElement obj = gson.toJsonTree(users, new TypeToken<List<User>>() {}.getType());

			sendMsg.add(ACTION, getAction());
			sendMsg.add("users", obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sendMsg;
	}
}
