package org.restaurant.server.action;

import java.io.PrintWriter;
import java.util.Map;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class RequestHandler {	
	private String action;
	private JsonElement data;
	private PrintWriter pw;
	private Map<Integer, PrintWriter> waiters;
	
	/**
	 * A kliens által küldött üzenetből kiszedi az action nevét.
	 * @param requestMsg kliens által küldött üzenet.
	 * @param pw kliens PrintWritere.
	 * @param scene felület scene.
	 */
	public RequestHandler(String requestMsg, PrintWriter pw, Map<Integer, PrintWriter> waiters) {
		JsonElement jelement = new JsonParser().parse(requestMsg);
		this.action = jelement.getAsJsonObject().get("action").getAsString();
		this.data = jelement;
		this.pw = pw;
		this.waiters = waiters;
	}
	
	//TODO maybe its better if it's return a response msg and the pw send that outside
	//separate db action, response and scene change
	public void process() {
		if("registration".equals(action)){
			new UserAction(data, pw).registration();
		} else if ("getGuestbooks".equals(action)){
			new UserAction(data, pw).getGuestbooks();
		} else if ("setIngredientAlerts".equals(action)) {
			new ProductAction(data, pw).setIngredientAlerts();
		} else if ("saveGuestbook".equals(action)){
			new UserAction(data, pw).saveGuestbook();
		} else if ("getGuestBookAdministrator".equals(action)) {
			new UserAction(data, pw).getGuestBookAdministrator();
		} else if("logIn".equals(action)) {
			new UserAction(data, pw).logIn();
		} else if("closeWaiter".equals(action)) {
			new UserAction(data, pw).closeWaiter(waiters);
		}else if("payGuest".equals(action)){
			new OrderAction(data, pw).payGuest();
		} else if("ordering".equals(action)) {
			new OrderAction(data, pw).getOrder();
		} else if("saveRequestOrdersByWaiter".equals(action)) {
			new OrderAction(data, pw).saveRequestOrdersByWaiter();
		} else if("saveProductByAdmin".equals(action)) {
			new ProductAction(data, pw).saveProductByAdmin();
		} else if("getProducts".equals(action)) {
			new ProductAction(data, pw).getProducts();
		} else if("deleteProduct".equals(action)) {
			new ProductAction(data, pw).deleteProduct();
		} else if("getIngredients".equals(action)) {
			new ProductAction(data, pw).getIngredients();
		} else if("getProductCategoryStatistics".equals(action))  {
			new ProductAction(data, pw).getProductCategoryStatistics();
		} else if("getProductIncomes".equals(action)) {
			new ProductAction(data, pw).getProductIncomes();
		} else if("getUsers".equals(action)) {
			new UserAction(data, pw).getUsers();
		} else if("registrationByAdmin".equals(action)) {
			new UserAction(data, pw).registrationByAdmin();
		} else if("getTable".equals(action)) {
			new CommonAction(data, pw).getTable();
		} else if("acceptPayByWaiter".equals(action)) {
			new OrderAction(data, pw).acceptPayByWaiter();
		} else if("uploadIngredients".equals(action)) {
			new ProductAction(data, pw).uploadIngredients();
		}
			
	}
}
