package org.restaurant.server.action;

import java.io.PrintWriter;

import org.apache.ibatis.session.SqlSession;
import org.restaurant.common.model.beans.Table;
import org.restaurant.server.data.controller.TableController;
import org.restaurant.server.data.mapper.CommonMapper;
import org.restaurant.server.data.mapper.MyBatisFactory;
import org.restaurant.server.exception.controller.MissingBeanControllerException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CommonAction extends AbstractAction {

	public CommonAction(JsonElement data, PrintWriter pw) {
		super(data, pw);
	}

	public void getTable() {
		Integer tableId = rawData.getAsJsonObject().get(DATA).getAsInt();

		try (SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			CommonMapper commonMapper = session.getMapper(CommonMapper.class);
			TableController controller;
			
				controller = new TableController(commonMapper, tableId);
			
			// TODO throw warning
			if (controller.getBean() != null) {
				JsonObject sendMsg = createTableJsonMsg("getTable", controller.getBean());
				pw.println(sendMsg.toString());
				pw.flush();
			}
		} catch (MissingBeanControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private JsonObject createTableJsonMsg(String actionType, Table table) {

		Gson gson = new GsonBuilder().create();
		String json = gson.toJson(table);
		JsonObject obj = new JsonParser().parse(json).getAsJsonObject();

		JsonElement actionNameJsonElement = new JsonParser().parse(actionType);
		JsonObject sendMsg = new JsonObject();

		sendMsg.add(ACTION, actionNameJsonElement);
		sendMsg.add(DATA, obj);
		return sendMsg;
	}
}
