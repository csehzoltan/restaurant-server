package org.restaurant.server.socket;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.restaurant.server.action.RequestHandler;
import org.restaurant.server.properties.PropertyException;
import org.restaurant.server.properties.ServerConfig;

/**
 * Szerver oldali kommunikációért felelős, ellenőrzi és jóváhagyja a kliens kapcsolódását, kérésteket fogad, a kért 
 * műveleteket végrehajta és választ küld amennyiben szükséges.
 * @author Zolt�n
 *
 */
public class RestaurantServer implements Runnable {
	
	private List<Socket> clients = new ArrayList<>();
	private ServerSocket serverSocket;
	private static Map<Integer, PrintWriter> waiters = Collections.synchronizedMap(new HashMap<>());
	
	@Override
	public void run() {
		ServerConfig serverConfig = null;
		try {
			serverConfig = new ServerConfig();
		} catch (PropertyException e1) {
			e1.printStackTrace();
		}
		try {
			serverSocket = new ServerSocket(serverConfig != null ? serverConfig.getPort() : 7898);
			acceptClients();
		} catch (IOException e) {
			e.getMessage();
		} finally {
			if(serverSocket!= null && !serverSocket.isClosed()) {
				try {
					serverSocket.close();
				} catch (IOException e) {
					e.getMessage();
				}
			}
		}
	}

	private void acceptClients() throws IOException {
		while (!serverSocket.isClosed()) {
				Socket socket = serverSocket.accept();
				synchronized (clients) {
					Communication customer = new Communication(socket);
					customer.start();
					clients.add(socket);
				}
		}
	}
	
	/**
	 * Leállítja a szerver és a kliens socketeket.
	 */
	public void shutDown() {
		try {
			if(serverSocket != null) {
				serverSocket.close();	
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		synchronized (clients) {
			for(Socket s : clients) {
				try {
					s.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Kommunikációs szál a szerver és a kliens között, melyen keresztül az üzenetváltás történik.
	 * @author cseh.zoltan
	 *
	 */
	private class Communication extends Thread {
		private Socket socket;
		
		public Communication(Socket socket) {
			this.socket = socket;
		}
		
		@Override
		public void run() {
			try (
				Scanner reader = new Scanner(socket.getInputStream());
				PrintWriter pw = new PrintWriter(socket.getOutputStream());) {
				while(reader.hasNextLine()) {
					processRequest(reader, pw);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		private void processRequest(Scanner reader, PrintWriter pw) {
			try {
				String message = reader.nextLine();
				RequestHandler requestHandler = new RequestHandler(message, pw, waiters);
				requestHandler.process();
			} catch (Exception e) {
				//TODO Server alert
				System.out.print(e.getMessage());
			}
		}
	}
	
	/**
	 * Felszolgáló PrintWriter hozzáadása a waiters maphez.
	 * @param userId felhasználó id-ja.
	 * @param pw felszolgálóhoz tartozó PrintWriter
	 */
	public static void addWaiter(Integer userId, PrintWriter pw) {
		waiters.put(userId, pw);
	}
	
	/**
	 * Felszolgáló törlése a waiter mapből.
	 * @param userId felszolgáló id-ja.
	 */
	public static void removeWaiter(Integer userId) {
		waiters.remove(userId);
	}
	
	/**
	 * Visszatér a felszolgálók mappel (kulcsa: felhasználó id, értéke a felszolgáló PrintWritere).
	 * @return felszolgálók PrintWriter mapje.
	 */
	public static Map<Integer, PrintWriter> getWaiters() {
		return RestaurantServer.waiters;
	}
}
