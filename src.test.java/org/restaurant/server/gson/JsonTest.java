package org.restaurant.server.gson;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.restaurant.common.json.JsonHelper;
import org.restaurant.common.model.beans.Chair;
import org.restaurant.common.model.beans.PayType;
import org.restaurant.common.model.beans.Product;
import org.restaurant.common.model.beans.ProductOrder;
import org.restaurant.common.model.beans.ProductPrice;
import org.restaurant.common.model.beans.ProductRequest;
import org.restaurant.common.model.beans.User;
import org.restaurant.common.model.beans.UserType;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class JsonTest {
	
	@Test
	public void jsonActonRequest() throws ParseException {

		SimpleDateFormat dateformat = new SimpleDateFormat("dd/mm/yyyy");
		Date date = dateformat.parse("17/07/1989");

		assertTrue("{\"action\":\"getGuestBookAdministrator\",\"data\":198907}".equals(
				JsonHelper.jsonActonRequest("getGuestBookAdministrator", new SimpleDateFormat("yyyymm").format(date))
						.toString()));
	}

	@Test
	public void createJsonMsg() throws ParseException {
		User user = new User();
		user.setAccountName("User name");
		user.setPassword("pass");
		user.setType(UserType.GUEST);
		user.setDeleted(false);
		ProductRequest request = new ProductRequest();
		request.setId(1);
		request.setPayType(PayType.ONEPAYS);
		request.setProductOrders(null);
		SimpleDateFormat dateformat = new SimpleDateFormat("dd/mm/yyyy");
		Date date = dateformat.parse("17/07/2019");
		request.setRequestedAt(date);
		request.setDeleted(false);
		List<ProductRequest> prodReqList = new ArrayList<>();
		prodReqList.add(request);
		user.setProductRequests(prodReqList);

		assertTrue(
				"{\"action\":\"saveUser\",\"data\":{\"accountName\":\"User name\",\"password\":\"pass\",\"type\":\"GUEST\",\"productRequests\":[{\"requestedAt\":\"Jan 17, 2019 12:07:00 AM\",\"payType\":\"ONEPAYS\",\"id\":1,\"deleted\":false}],\"deleted\":false}}"
						.equals(JsonHelper.createJsonMsg("saveUser", user, null)));

	}
	
	@Test
	public void createJsonMsgInfo() {
		User user = new User();
		user.setId(1);
		user.setName("Test Name");

		Map<String, String> payParams = new HashMap<>();
		payParams.put("tableId", "1");
		payParams.put("payTypeId", "1");

		assertTrue(
				"{\"action\":\"saveUser\",\"data\":{\"name\":\"Test Name\",\"id\":1,\"deleted\":false},\"info\":{\"payTypeId\":\"1\",\"tableId\":\"1\"}}"
						.equals(JsonHelper.createJsonMsg("saveUser", user, payParams)));
	}
	
	@Test
	public void createJsonMsgList() {

		List<ProductOrder> modifiedProductOrders = new ArrayList<>();
		ProductOrder productOrder1 = new ProductOrder();
		productOrder1.setId(1);
		Chair chair = new Chair();
		chair.setId(1);
		chair.setName("C1");
		chair.setDeleted(false);
		ProductPrice price = new ProductPrice();
		price.setAmount(1.2);
		price.setDeleted(false);
		price.setId(1);
		price.setPrice(20.5);
		Product prod = new Product();
		prod.setId(1);
		prod.setName("Product name");
		price.setProduct(prod);

		productOrder1.setProductPrice(price);
		productOrder1.setChair(chair);

		ProductOrder productOrder2 = new ProductOrder();
		productOrder2.setId(1);
		Chair chair2 = new Chair();
		chair2.setId(1);
		chair2.setName("C1");
		chair2.setDeleted(false);
		ProductPrice price2 = new ProductPrice();
		price2.setAmount(1.2);
		price2.setDeleted(false);
		price2.setId(1);
		price2.setPrice(20.5);
		Product prod2 = new Product();
		prod2.setId(1);
		prod2.setName("Product name");
		price2.setProduct(prod2);

		productOrder2.setProductPrice(price2);
		productOrder2.setChair(chair2);

		modifiedProductOrders.add(productOrder1);
		modifiedProductOrders.add(productOrder2);

		assertTrue(
				"{\"action\":\"saveRequestOrdersByWaiter\",\"data\":[{\"productPrice\":{\"product\":{\"name\":\"Product name\",\"id\":1,\"deleted\":false},\"amount\":1.2,\"price\":20.5,\"id\":1,\"deleted\":false},\"chair\":{\"name\":\"C1\",\"id\":1,\"deleted\":false},\"rejectedQuantity\":0,\"id\":1,\"deleted\":false},{\"productPrice\":{\"product\":{\"name\":\"Product name\",\"id\":1,\"deleted\":false},\"amount\":1.2,\"price\":20.5,\"id\":1,\"deleted\":false},\"chair\":{\"name\":\"C1\",\"id\":1,\"deleted\":false},\"rejectedQuantity\":0,\"id\":1,\"deleted\":false}]}"
						.equals(JsonHelper.createJsonMsgList("saveRequestOrdersByWaiter", modifiedProductOrders,
								ProductOrder.class).toString()));

	}

	@Test
	public void testRequestActionMsg() {
		String requestMsg = "{\"action\":\"registrationByAdmin\",\"error\":\"Username already exists! Please choose an other username!\"}";
		JsonElement jelement = new JsonParser().parse(requestMsg);
		assertTrue("registrationByAdmin".equals(jelement.getAsJsonObject().get("action").getAsString()));
	}
	
	@Test
	public void testRequestDataMsg() {
		String intJson = "{\"action\":\"getTable\",\"data\":1}";
		JsonElement jelement = new JsonParser().parse(intJson);
		assertTrue("1".equals(jelement.getAsJsonObject().get("data").getAsString()));
	}
	
}
