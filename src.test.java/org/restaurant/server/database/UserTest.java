package org.restaurant.server.database;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.restaurant.common.model.beans.User;
import org.restaurant.common.model.beans.UserType;
import org.restaurant.server.data.controller.UserController;
import org.restaurant.server.data.mapper.MyBatisFactory;
import org.restaurant.server.data.mapper.UserMapper;

public class UserTest {

	@Test
	public void testUserLoadByLogInData() {
		try (SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			UserMapper userMapper = session.getMapper(UserMapper.class);
			User user = new User();
			user.setName("waiter");
			user.setAccountName("waiter");
			user.setPassword("pass");
			user.setType(UserType.WAITER);
			UserController userController = new UserController(userMapper, user);
			Map<String, Object> filter = new HashMap<>();
			filter.put("loginCheck", true);
			userController.getUserByLoginData(user, filter);
			user = userController.getBean();
			
			assertTrue(user.getId() != null);
		}
	}

}
