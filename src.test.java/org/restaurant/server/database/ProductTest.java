package org.restaurant.server.database;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.restaurant.common.model.beans.Product;
import org.restaurant.server.data.controller.ProductController;
import org.restaurant.server.data.mapper.MyBatisFactory;
import org.restaurant.server.data.mapper.ProductMapper;

public class ProductTest {
	
	@Test
	public void loadOnlyNotDeletedProducts() {
		try (SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			ProductMapper productMapper = session.getMapper(ProductMapper.class);
			ProductController productController = new ProductController(productMapper);
			Map<String, Object> productFilter = new HashMap<>();
			productFilter.put("showOnlyNotDeleted", true);
			List<Product> products = productController.getProducts(productFilter);
			
			for(Product product : products) {
				assertTrue(!product.isDeleted());
			}
		}
	}
}
