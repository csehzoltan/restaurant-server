package org.restaurant.server.database;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.restaurant.common.model.beans.Product;
import org.restaurant.common.model.beans.ProductPrice;
import org.restaurant.common.model.beans.Size;
import org.restaurant.common.model.beans.Table;
import org.restaurant.server.data.controller.ProductController;
import org.restaurant.server.data.controller.ProductPriceController;
import org.restaurant.server.data.controller.TableController;
import org.restaurant.server.data.mapper.CommonMapper;
import org.restaurant.server.data.mapper.MyBatisFactory;
import org.restaurant.server.data.mapper.ProductMapper;
import org.restaurant.server.exception.controller.MissingBeanControllerException;

public class CommonTest {

	@Test
	public void testConnection() {
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			assertTrue(session != null && session.getConnection() != null);
		}
	}
	
	@Test
	public void testConnectionAndSimpleTableLoad() throws MissingBeanControllerException {
		try (SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			CommonMapper commonMapper = session.getMapper(CommonMapper.class);
			TableController controller = new TableController(commonMapper, 1);
			Table table = controller.getBean();
			assertTrue("1".equals(table.getId().toString()));
			assertTrue("T1".equals(table.getName()));
		}
	}

	@Test
	public void typeHandler() throws MissingBeanControllerException {
		try(SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			ProductMapper productMapper = session.getMapper(ProductMapper.class);
			ProductPriceController priceController = new ProductPriceController(productMapper, 1);
			ProductPrice price = priceController.getBean();
			assertTrue(price.getSize() != null && (price.getSize() == Size.LARGE || price.getSize() == Size.MEDIUM || price.getSize() == Size.SMALL));
		}
	}
	
	@Test
	public void mapFilterTest() {
		try (SqlSession session = MyBatisFactory.getSqlSessionFactory().openSession()) {
			ProductMapper productMapper = session.getMapper(ProductMapper.class);
			ProductController productController = new ProductController(productMapper);
			Map<String, Object> productFilter = new HashMap<>();
			productFilter.put("showOnlyNotDeleted", true);
			List<Product> products = productController.getProducts(productFilter);
			
			for(Product product : products) {
				assertTrue(!product.isDeleted());
			}
		}
	}
}
